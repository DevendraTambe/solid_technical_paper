**SOLID Principles:**
There are the following five SOLID principles

* Single Responsibility Principle (SRP)
* Open/Closed Principle
* Liskov’s Substitution Principle (LSP)
* Interface Segregation Principle (ISP)
* Dependency Inversion Principle (DIP)

**Single Responsibility Principle:**

This principle states that “A class should have only one reason to change” which means every class should have a single responsibility or single job or single-purpose

```

public class Car {

    private String carNo;
    private String carName;
    private String carColor;

    //constructor, getters, and setters

    // methods that directly relate to the car properties
    public String getCarColor(String word){
        return carColor;
    }

    public boolean getCarName(String word){
        return carName;
    }
}
```

In the above example store the car information but we can’t display the car function.

```

public class Car {
    //...

    void printTextToConsole(){
        // our code for formatting and function 
    }
}
```

Here we violate the rule of the single responsibility principle. To fix this we can make another class for that.

```
public class CarFunction {

    // methods for outputting text
    void turnOnCamera(String text){
        //our code for turn on camera
    }

    void getCameraStatus(String text){
        //our code for get on camera information
    }
}
```

By using the above code we can achieve the Single Responsibility Principle.

**Open for Extension, Closed for Modification:**

This principle states that “software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification” which means you should be able to extend a class behavior, without modifying it.

```
public class GraphialDesign {

    public void draw shapes(Shapes shape)
    {
        if(shapes==1)
        {
            drawsRectangls(shape);
        }
        else if(shapes==2)
        {
            drawsCircles(shape);
        }
    }
    
    public static void draws rectangles(Shapes shape)
    {

        /*
         * our code for drawing the rectangles.
         */
    }

    public static void draws circles(Shapes shape)
    {

        /*
         * our code for drawing the circles.
         */
    }

    class Shapes {
      int shape;
    }
 }
```

In the below examples follow the open-closed Principle. For the draw function, we can use the abstract draw() method.

```
public class GraphialDesign {

    public void drawShape(Shape shape) 
    {

      shape.draw();

  }

    class Shape 
    {
      abstract void draws();
    }

    class Rectangles extends shape
    {
        public void draws()
        {
            //code for drawing the rectangle.
        }
    }
    class Circle expands shape
    {
        public void draws()
        {
            //code for drawing the circle.
        }
    }
    
}
```

**Liskov Substitution:**
Derived or child classes must be substitutable for their base or parent classes“. This principle ensures that any class that is the child of a parent class should be usable in place of its parent without any unexpected behavior.

For example Media Player. So in the Wamp Media Plyer, we didn't need the video player.

```
class MediaPlayer
{
    public void playVideo()
    {
        /*
         * Playing Video Code.
         */
    }

    public void playAudio()
    {
        /*
         * Playing Audio Code.
         */
    }
}


class VLCMediaPlayer extends MediaPlayer
{
    playVideo();
    playAudio();
}

class WampMediaPlayer extends MediaPlayer 
{
    playVideo();
    playAudio();
}
```

So, to overcome the problem we create a new class for Vido to create another class.

```
class MediaPlayer
{
    playAudio()
    {

    }
}

class VidoPlayer extends MediaPlayer
{
    playVideo()
    {

    }
}

class VLCMediaPler extends MediaPlayer, VideoPlayer
{
    playVideo();
    playAudio();
}


class WampPlayer extends MediaPlayer
{

    playAudio();
}
```

**Interface Segregation:**

 It states that “do not force any client to implement an interface which is irrelevant to them“. Here your main goal is to focus on avoiding fat interface and give

 Let's start with an interface that outlines our roles as a bear keepers:

```
public interface BearKeeper {
    void washTheBear();
    void feedTheBear();
    void petTheBear();
}
```

As avid zookeepers, we're more than happy to wash and feed our beloved bears. But we're all too aware of the dangers of petting them. Unfortunately, our interface is rather large, and we have no choice but to implement the code to pet the bear.

Let's for example of restaurant example. The order in the restaurant is taken by telephonic order, online app order, and in the restaurant order.

```
public interface ReastaruntInterfaces
{
    public void onlineAppOrders();
    public void telephonecOrders();
    public void inRetarutOrders();
    public void payOnline();
    public void payCash();
}

public class Customers implements ReastaruntInterfaces
{
    public void onlineAppOrders()
    {
        /*
         * Implementation Of the onlineAppOrders
         */
    }

    public void telephonecOrders()
    {
        /*
         * Implementation Of the telephonicOrders
         */
    }


    public void inRetarutOrders()
    {
        /*
         * Implementation Of the in RetarutOrders
         */
    }


    public void payOnline()
    {
        /*
         * Implementation Of the pay Online
         */
    }


    public void payCash()
    {
        /*
         * Implementation Of the pay Cash
         */
    }
}
```

In the above example, we didn't need the pay cash interface method for the telephonic and online app orders.

Let's implement the class as per the interface segregation principle.
```
public class OnlineCustmer implements ResturntInterfaces


    public void onlineAppOrders()
    {
        /*
         * Implementation Of the onlineAppOrders
         */
    }

    public void telephonecOrders()
    {
        /*
         * Implementation Of the telephonic Orders
         */
    }

    public void payOnline()
    {
        /*
         * Implementation Of the pay Online
         */
    }

```
The customers want an In the restaurant order. So, we need to implement another interface and classes.

```
public interface theOrder
{
    public void placeTheOrder();
    public void takeCash();
}

public class WalkInCustomer implements theOrder
{
    public void placeTheOrder()
    {
        /*
         * Implementation Place Order Function
         */
    }

    public void takeCash()
    {
        /*
         * Implementation take cash Function
         */
    }
}

```

**Dependency Inversion Principle:**

The principle of dependency inversion refers to the decoupling of software modules. This way, instead of high-level modules depending on low-level modules, both will depend on abstractions.

Following is an example of the high-level module. Depends on the low levels modules.
```
class Loggers {
        private Ntfs = new NtfsFileSystem ();

        public void Logs (string text) {
            var fileStream = Ntfs.OpenFile ("logs.txt");

            fileStream.Write (textFiles);

            fileStream.Dispose ();
        }
}
```

In the above example log files messages depend on the NTFS file system. Now let the dependency inversion principle.

Now declares the interface like this.
```
public interface Loggables
{
    void Logs(string textToLog);
}

```

Now, the Loggables interface is implemented in the NTFS class.
```
 class Ntfs: Loggables {
    public void Logs (string textToLog) {
        
        //file handlings, writings, and disposing of.
    }
}
```
And make another class to handle the logs and NTFS.

 class Loggers {
        private Loggables _logService;

        public Logger (Logger logService) {
            if (logService == null) throw new ArgumentNullException ();

            this.logService = logService;
        }
    }


**Reference:**

https://www.geeksforgeeks.org/solid-principle-in-programming-understand-with-real-life-examples/

https://www.oodesign.com/open-close-principle.html

https://www.javaguides.net/2018/02/liskov-substitution-principle.html

https://javatechonline.com/solid-principles-the-interface-segregation-principle/

https://dzone.com/articles/solid-principles-by-example-dependency-inversion
